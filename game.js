let now = performance.now();
let fpsCounts = [];

let canvas0 = document.getElementById("canvas0");
canvas0.width = window.innerWidth;
canvas0.height = window.innerHeight;
let ctx0 = canvas0.getContext("2d");

let canvas1 = document.getElementById("canvas1");
canvas1.width = window.innerWidth;
canvas1.height = window.innerHeight;
let ctx1 = canvas1.getContext("2d");

let seki = new Image(625, 625);
seki.src = "sekibanki.png";
let sekiMode = false;
let sekiCheck = 0;
let sekiKeys = ["KeyS", "KeyE", "KeyK", "KeyI", "KeyB", "KeyA", "KeyN", "KeyK", "KeyI"];

let optionButtons = [];
let teamColors = [4, 254];
let aimCenter = new Vector2(canvas0.width / 2, canvas0.height / 2);
let mouseOnScreen = new Vector2(0);
let screenScale = new Vector2(canvas0.height / 1080);

let currentMode = -1;
let game = new GameManager();
let ballWeight = 2;
let throwStrength = 2500;
let gravityMod = 1;
let moveSpeed = 500;
let jumpForce = -1200;

if (currentMode == 2) {game.startGame();}
let paused = false;

function loop (timestamp, again) {
    let dT = (performance.now() - now) / 1000;
    now = performance.now();

    canvas0.width = window.innerWidth;
    canvas0.height = window.innerHeight;
    canvas1.width = window.innerWidth;
    canvas1.height = window.innerHeight;
    screenScale = new Vector2(canvas0.height / 1080 < canvas0.width / 1920 ? canvas0.height / 1080 : canvas0.width / 1920);

    ctx0.clearRect(0, 0, canvas0.width, canvas0.height);
    ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
    ctx0.fillStyle = "#444444";
    ctx0.fillRect(0, 0, canvas0.width, canvas0.height);

    if (sekiMode) {
        ctx0.drawImage(seki, 0, 0, canvas0.width, canvas0.height);
    }

    if (currentMode == -1) {
        ctx0.translate(canvas0.width / 2, canvas0.height / 2);
        ctx0.scale(screenScale.x, screenScale.y);

        ctx0.fillStyle = "#000000";
        ctx0.fillRect(-100, 0, 200, 75);
        ctx0.fillRect(-125, 100, 250, 75);
        ctx0.fillRect(-300, 200, 600, 75);

        ctx0.fillStyle = "#ffffff";
        ctx0.font = "normal 128px sans-serif";
        ctx0.fillText("Goofy", -200, -300);
        ctx0.fillText("Dodgeball", -250, -150);

        ctx0.font = "normal 48px sans-serif";
        ctx0.fillText("Play", -50, 50);
        ctx0.fillText("Controls", -100, 150);
        ctx0.fillText("View the source code", -250, 250);

        ctx0.font = "normal 24px sans-serif";
        ctx0.fillText("Even if you know Javascript and how to work with the html canvas, you'll still get confused", -450, 300);
    } else if (currentMode == 0) {
        ctx0.translate(canvas0.width / 2, canvas0.height / 2);
        ctx0.scale(screenScale.x, screenScale.y);

        ctx0.fillStyle = "#ffffff";
        ctx0.font = "normal 48px sans-serif";
        ctx0.fillText("A and D moves Left and Right", -300, -350);
        ctx0.fillText("S \"crouches\"", -125, -300);
        ctx0.fillText("Space jumps", -125, -150);
        ctx0.fillText("Use the mouse or arrow keys to aim", -400, -200);
        ctx0.fillText("When not holding a ball hold right click", -400, -100);
        ctx0.fillText("to pick up or catch one", -200, -50);

        ctx0.fillText("When holding a ball, hold right click to block", -450, 50);
        ctx0.fillText("or hold left click to charge up a throw", -300, 100);
        ctx0.fillText("and release left click to throw", -300, 150);

        ctx0.fillText("you can also use q to toggle block/pick up", -400, 250);
        ctx0.fillText("and shift to throw", -200, 300);

        ctx0.fillStyle = "#000000";
        ctx0.fillRect(-100, 400, 200, 75);

        ctx0.fillStyle = "#ffffff";
        ctx0.font = "normal 48px sans-serif";
        ctx0.fillText("Back", -50, 450);
    } else if (currentMode == 1) {
        ctx0.translate(canvas0.width / 2, canvas0.height / 2);
        ctx0.scale(screenScale.x, screenScale.y);

        ctx0.fillStyle = "#000000";
        ctx0.fillRect(-100, -100, 200, 75);
        ctx0.fillRect(-100, 0, 200, 75);

        optionButtons.forEach(button => {
            ctx0.fillRect(button.position.x, button.position.y, button.size.x, button.size.y);
        });

        ctx0.fillStyle = "#ffffff";
        ctx0.font = "normal 48px sans-serif";
        ctx0.fillText("Start", -50, -50);
        ctx0.fillText("Back", -50, 50);

        optionButtons.forEach(button => {
            ctx0.fillText(button.text, button.position.x + button.size.x / 3, button.position.y + button.size.y * 2 / 3);
        });

        ctx0.fillText("Score Mode", -600, -450);
        ctx0.fillText(game.scoreMode == 0 ? "time" : game.scoreMode == 1 ? "score" : "score w/ time", -600, -400);
        ctx0.fillText("Points to Win", -600, -300);
        ctx0.fillText(game.pointsToWin, -600, -250);
        ctx0.fillText("Game Length", -600, -150);
        ctx0.fillText(timeConversion(game.matchTimer), -600, -100);
        ctx0.fillText("Red Players", -600, -0);
        ctx0.fillText(game.teamAPlayerCount, -600, 50);
        ctx0.fillText("Blue Players", -600, 150);
        ctx0.fillText(game.teamBPlayerCount, -600, 200);
        ctx0.fillText("Your Team", -600, 300);
        ctx0.fillText(game.humanTeam == 0 ? "red" : "blue", -600, 350);

        ctx0.fillText("Fun settings :D", 300, -450);
        ctx0.fillText("Ball Weight", 300, -300);
        ctx0.fillText(ballWeight, 300, -250);
        ctx0.fillText("Throw Power", 300, -150);
        ctx0.fillText(throwStrength, 300, -100);
        ctx0.fillText("Gravity Mod", 300, -0);
        ctx0.fillText(gravityMod.toFixed(1), 300, 50);
        ctx0.fillText("Move Speed", 300, 150);
        ctx0.fillText(moveSpeed, 300, 200);
        ctx0.fillText("Jump Force", 300, 300);
        ctx0.fillText(jumpForce, 300, 350);
        ctx0.fillText("Ball Multiplier", 300, 450);
        ctx0.fillText(game.ballMultiplier, 300, 500);    
    } else if (currentMode == 2 && (document.hasFocus() || dT < 0.05)) {
        ctx0.scale(screenScale.x, screenScale.y);

        let center = new Vector2(canvas0.width / (2 * screenScale.x), canvas0.height / (2 * screenScale.y));
        aimCenter = new Vector2(0);
        let humans = 0;
        game.players.forEach(player => { 
            if (player.human) {
                aimCenter.addS(center.subR(player.focusPoint.position).subR(player.aim.divR(screenScale).subR(center)));
                humans++;
            }
        });
        aimCenter.divS(new Vector2(humans));
        if (humans == 0) { ctx0.translate(500, 600); }
        else { ctx0.translate(aimCenter.x, aimCenter.y); }

        if (dT < 0.5) {
            doPhysics(dT);
            game.players.forEach(player => {
                player.move(dT);
            });
        }

        draw(dT);
        game.gameLogic(dT);

        ctx1.fillStyle = "#ffffff";
        ctx1.font = "normal 48px sans-serif";
        //ctx1.fillText("fps: " + getAvgFps(dT).toFixed(2), 10, 50);
        //ctx1.fillText("Circles: " + circles.length, 10, 100);

        /*ctx1.strokeStyle = "#ff00ff";
        ctx1.lineWidth = 3;
        ctx1.beginPath();
        game.map.teamANav.forEach(nav => {
            let pos = worldToScreen(nav.position);
            ctx1.moveTo(pos.x, pos.y);
            pos = worldToScreen(new Vector2(nav.position.x + nav.length, nav.position.y));
            ctx1.lineTo(pos.x, pos.y);
        });
        game.map.teamBNav.forEach(nav => {
            let pos = worldToScreen(nav.position);
            ctx1.moveTo(pos.x, pos.y);
            pos = worldToScreen(new Vector2(nav.position.x + nav.length, nav.position.y));
            ctx1.lineTo(pos.x, pos.y);
        });
        ctx1.stroke();*/
    } else if (currentMode == 3) {
        ctx0.scale(screenScale.x, screenScale.y);

        
    }

    if (!paused && !game.gameOver) window.requestAnimationFrame(loop);
    if (paused) {
        ctx1.translate(0, canvas0.height / 2);
        ctx1.fillStyle = "#00000030";
        ctx1.fillRect(-99999, -999999, 99999999, 99999999);
        ctx1.fillStyle = "#ffffff";
        ctx1.font = "normal 48px sans-serif";
        ctx1.fillText("you paused", -100, 0);

        ctx1.font = "normal 32px sans-serif";
        ctx1.fillText("(or something)", -100, 50);

        ctx1.font = "normal 16px sans-serif";

        ctx1.font = "normal 48px sans-serif";
        ctx1.fillText("press ESC to unpause", -250, 125);
    }

    if (game.gameOver) {
        ctx1.translate(0, canvas0.height / 2);
        ctx1.fillStyle = "#00000030";
        ctx1.fillRect(-99999, -999999, 99999999, 99999999);
        ctx1.fillStyle = "#ffffff";
        ctx1.font = "normal 48px sans-serif";

        if (game.winner == 0) {
            ctx1.fillText("Red team wins!", -100, 0);
        }

        if (game.winner == 1) {
            ctx1.fillText("Blue team wins!", -100, 0);
        }

        if (game.winner == 2) {
            ctx1.fillText("It's a tie!", -100, 0);
        }

        ctx1.fillStyle = "#000000";
        ctx1.fillRect(-100, 300, 200, 75);

        ctx1.fillStyle = "#ffffff";
        ctx1.font = "normal 48px sans-serif";
        ctx1.fillText("Back", -50, 350);

        ctx0.setTransform(ctx1.getTransform());
    }
}

function draw(dT) {
    let gradient = ctx0.createLinearGradient(0, 0, game.map.center * 2, 0);
    gradient.addColorStop(0, "#44444400");
    gradient.addColorStop(0.495, `hsl(${teamColors[0]}, 25%, 25%)`);
    gradient.addColorStop(0.505, `hsl(${teamColors[1]}, 25%, 25%)`);
    gradient.addColorStop(1, "#44444400");
    ctx0.fillStyle = gradient;
    ctx0.fillRect(0, -500, game.map.center * 2, 3000);

    for (let i = 0; i < barriers.length; i++) {
        let barrier = barriers[i];

        if (barrier.specialColor == false) {
            ctx0.fillStyle = "#000000";
        } else {
            ctx0.fillStyle = barrier.specialColor;
        }
        ctx0.fillRect(barrier.position.x, barrier.position.y, barrier.size.x, barrier.size.y);
    }

    game.players.forEach(player => {
        if (!player.human) player.draw();
    });

    game.players.forEach(player => { 
        if (player.human) player.draw();
    });

    for (let i = 0; i < game.dodgeballs.length; i++) {
        let dodgeball = game.dodgeballs[i];

        dodgeball.draw();
    }
}

document.addEventListener("keydown", e => {
    game.players.forEach(player => { 
        if (player.human) {
            if (!e.repeat) {
                switch (e.code) {
                    case "KeyW":
                        player.movementKeys.w = 1;
                        break;
        
                    case "KeyA":
                        player.movementKeys.a = 1;
                        break;
        
                    case "KeyS":
                        player.movementKeys.s = 1;
                        break;
        
                    case "KeyD":
                        player.movementKeys.d = 1;
                        break;
        
                    case "Space":
                        player.movementKeys.j = 1;
                        if (player.ragdolled) {
                            player.ragdoll(false);
                        }
                        break;

                    case "ControlLeft":
                        player.ragdoll(!player.ragdolled);
                        break;

                    case "ShiftLeft":
                        player.indiArmAction[0] = true;
                        break;

                    case "KeyQ":
                        player.indiArmAction[2] = !player.indiArmAction[2];
                        break;

                    case "ArrowUp":
                        player.aimKeys.u = -1;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowDown":
                        player.aimKeys.d = 1;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowLeft":
                        player.aimKeys.l = -1;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowRight":
                        player.aimKeys.r = 1;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;

                    case "Escape":
                        if (!game.gameOver) {
                            paused = !paused;
                            now = performance.now();
                            window.requestAnimationFrame(loop);
                        }
                        break;
                }
            }
        }
    });

    if (!e.repeat) {
        if (e.code == sekiKeys[sekiCheck]) {
            sekiCheck++;

            if (sekiCheck == sekiKeys.length) {
                sekiMode = true;
            }
        }
    }
});

document.addEventListener("keyup", e => {
    game.players.forEach(player => { 
        if (player.human) {
            if (!e.repeat) {
                switch (e.code) {
                    case "KeyW":
                        player.movementKeys.w = 0;
                        break;
        
                    case "KeyA":
                        player.movementKeys.a = 0;
                        break;
        
                    case "KeyS":
                        player.movementKeys.s = 0;
                        break;
        
                    case "KeyD":
                        player.movementKeys.d = 0;
                        break;
        
                    case "Space":
                        player.movementKeys.j = 0;
                        break;

                    case "ShiftLeft":
                        player.indiArmAction[0] = false;
                        break;

                    case "ArrowUp":
                        player.aimKeys.u = 0;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowDown":
                        player.aimKeys.d = 0;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowLeft":
                        player.aimKeys.l = 0;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
        
                    case "ArrowRight":
                        player.aimKeys.r = 0;
                        player.aimAt(new Vector2(canvas0.width / 2 + player.aimKeys.l * 500 + player.aimKeys.r * 500, canvas0.height / 2 + player.aimKeys.u * 250 + player.aimKeys.d * 250));
                        break;
                }
            }
        }
    });
});

document.addEventListener("mousemove", e => {
    game.players.forEach(player => { 
        if (player.human) {
            player.aimAt(new Vector2(e.clientX, e.clientY));
        }
    });

    mouseOnScreen = new Vector2(e.clientX, e.clientY);

    if (currentMode == -1) {
        document.body.style.cursor = 'unset';

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 0)), new Vector2(200, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-125, 100)), new Vector2(250, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-300, 200)), new Vector2(600, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }
    } else if (currentMode == 0) {
        document.body.style.cursor = 'unset';

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 400)), new Vector2(200, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }
    } else if (currentMode == 1) {
        document.body.style.cursor = 'unset';

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, -100)), new Vector2(200, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 0)), new Vector2(200, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }

        optionButtons.forEach(button => {
            if (pointInRect(mouseOnScreen, worldToScreen(button.position), button.size.mulR(screenScale))) {
                document.body.style.cursor = 'pointer';
            }
        });
    }

    if (game.gameOver) {
        document.body.style.cursor = 'unset';

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 300)), new Vector2(200, 75).mulR(screenScale))) {
            document.body.style.cursor = 'pointer';
        }
    }
});

document.addEventListener("mousedown", e => {
    game.players.forEach(player => { 
        if (player.human) {
            if (e.button == 0) {
                player.indiArmAction[0] = true;
            } else if (e.button == 2) {
                player.indiArmAction[2] = true;
            }
        }
    });

    if (currentMode == -1) {
        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 0)), new Vector2(200, 75).mulR(screenScale))) {
            currentMode = 1;
            document.body.style.cursor = 'unset';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-125, 100)), new Vector2(250, 75).mulR(screenScale))) {
            currentMode = 0;
            document.body.style.cursor = 'unset';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-300, 200)), new Vector2(600, 75).mulR(screenScale))) {
            window.location.href = "https://gitlab.com/givowo/dodgeball-game-for-physics";
            document.body.style.cursor = 'unset';
        }
    } else if (currentMode == 0) {
        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 400)), new Vector2(200, 75).mulR(screenScale))) {
            currentMode = -1;
            document.body.style.cursor = 'unset';
        }
    } else if (currentMode == 1) {
        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, -100)), new Vector2(200, 75).mulR(screenScale))) {
            currentMode = 2;
            game.startGame();
            document.body.style.cursor = 'unset';
        }

        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 0)), new Vector2(200, 75).mulR(screenScale))) {
            currentMode = -1;
            document.body.style.cursor = 'unset';
        }

        optionButtons.forEach(button => {
            if (pointInRect(mouseOnScreen, worldToScreen(button.position), button.size.mulR(screenScale))) {
                button.click();
            }
        });
    }

    if (game.gameOver) {
        if (pointInRect(mouseOnScreen, worldToScreen(new Vector2(-100, 300)), new Vector2(200, 75).mulR(screenScale))) {
            currentMode = 1;
            game.reset();
            window.requestAnimationFrame(loop);
            document.body.style.cursor = 'unset';
        }
    }
});

document.addEventListener("mouseup", e => {
    game.players.forEach(player => { 
        if (player.human) {
            if (e.button == 0) {
                player.indiArmAction[0] = false;
            } else if (e.button == 2) {
                player.indiArmAction[2] = false;
            }
        }
    });
});

document.addEventListener("contextmenu", function (e) {
    e.preventDefault();
}, false);


function disabledEvent(e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    } else if (window.event) {
        window.event.cancelBubble = true;
    }
    e.preventDefault();
    return false;
}

function getAvgFps(dT) {
    fpsCounts.push((1 / dT));

    if (fpsCounts.length > 8) {
        fpsCounts.splice(0, 1);
    }

    let fps = 0;

    fpsCounts.forEach(e => {
        fps += e;
    });

    return (fps / fpsCounts.length);
}

let pipes = [];
for (let i = 0; i < 20; i++) {
    pipes.push(new Audio("pipe1.ogg"));
}

let nextPipe = 0;
let pipe = false;
function playPipe() {
    if (!pipe) return;
    pipes[nextPipe].pause();
    pipes[nextPipe].fastSeek(0);
    pipes[nextPipe].play();

    nextPipe = (nextPipe + 1) % pipes.length;
}

function timeConversion(duration) {
    const portions = [];

    const msInMinute = 1000 * 60;
    const minutes = Math.trunc(duration / msInMinute);
    portions.push(minutes + ':');
    duration = duration - (minutes * msInMinute);
  
    const msInSecond = 1000;
    const seconds = Math.trunc(duration / msInSecond);
    portions.push(seconds + '');
    duration = duration - (seconds * msInSecond);
  
    return portions.join('');
}

window.requestAnimationFrame(loop);

// Making the options menu
optionButtons.push(new MenuButton(new Vector2(-675, -450), new Vector2(50, 50), "<", () => {game.scoreMode = clamp(game.scoreMode - 1, 0, 2);}));
optionButtons.push(new MenuButton(new Vector2(-300, -450), new Vector2(50, 50), ">", () => {game.scoreMode = clamp(game.scoreMode + 1, 0, 2);}));
optionButtons.push(new MenuButton(new Vector2(-300, -300), new Vector2(50, 50), "+", () => {game.pointsToWin++;}));
optionButtons.push(new MenuButton(new Vector2(-675, -300), new Vector2(50, 50), "-", () => {game.pointsToWin--;}));
optionButtons.push(new MenuButton(new Vector2(-300, -150), new Vector2(50, 50), "+", () => {game.matchTimer += 30000;}));
optionButtons.push(new MenuButton(new Vector2(-675, -150), new Vector2(50, 50), "-", () => {game.matchTimer -= 30000;}));
optionButtons.push(new MenuButton(new Vector2(-300, 0), new Vector2(50, 50), "+", () => {game.teamAPlayerCount++;}));
optionButtons.push(new MenuButton(new Vector2(-675, 0), new Vector2(50, 50), "-", () => {game.teamAPlayerCount--;}));
optionButtons.push(new MenuButton(new Vector2(-300, 150), new Vector2(50, 50), "+", () => {game.teamBPlayerCount++;}));
optionButtons.push(new MenuButton(new Vector2(-675, 150), new Vector2(50, 50), "-", () => {game.teamBPlayerCount--;}));
optionButtons.push(new MenuButton(new Vector2(-300, 75), new Vector2(50, 50), "+", () => {game.teamAPlayerCount++; game.teamBPlayerCount++;}));
optionButtons.push(new MenuButton(new Vector2(-675, 75), new Vector2(50, 50), "-", () => {game.teamAPlayerCount--; game.teamBPlayerCount--;}));
optionButtons.push(new MenuButton(new Vector2(-675, 300), new Vector2(50, 50), "<", () => {game.humanTeam = clamp(game.humanTeam - 1, 0, 1);}));
optionButtons.push(new MenuButton(new Vector2(-300, 300), new Vector2(50, 50), ">", () => {game.humanTeam = clamp(game.humanTeam + 1, 0, 1);}));
optionButtons.push(new MenuButton(new Vector2(600, -300), new Vector2(50, 50), "+", () => {ballWeight++;}));
optionButtons.push(new MenuButton(new Vector2(225, -300), new Vector2(50, 50), "-", () => {ballWeight--;}));
optionButtons.push(new MenuButton(new Vector2(600, -150), new Vector2(50, 50), "+", () => {throwStrength += 100;}));
optionButtons.push(new MenuButton(new Vector2(225, -150), new Vector2(50, 50), "-", () => {throwStrength -= 100;}));
optionButtons.push(new MenuButton(new Vector2(600, 0), new Vector2(50, 50), "+", () => {gravityMod += 0.1;}));
optionButtons.push(new MenuButton(new Vector2(225, 0), new Vector2(50, 50), "-", () => {gravityMod -= 0.1;}));
optionButtons.push(new MenuButton(new Vector2(600, 150), new Vector2(50, 50), "+", () => {moveSpeed += 50;}));
optionButtons.push(new MenuButton(new Vector2(225, 150), new Vector2(50, 50), "-", () => {moveSpeed -= 50;}));
optionButtons.push(new MenuButton(new Vector2(600, 300), new Vector2(50, 50), "+", () => {jumpForce -= 100;}));
optionButtons.push(new MenuButton(new Vector2(225, 300), new Vector2(50, 50), "-", () => {jumpForce += 100;}));
optionButtons.push(new MenuButton(new Vector2(600, 450), new Vector2(50, 50), "+", () => {game.ballMultiplier++;}));
optionButtons.push(new MenuButton(new Vector2(225, 450), new Vector2(50, 50), "-", () => {game.ballMultiplier--;}));