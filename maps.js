let basic1 = new Map(2000,
[
    new Barrier(new Vector2(0, 2000), new Vector2(4000, 50)), // Bottom 0
    new Barrier(new Vector2(0, -500), new Vector2(50, 2250)), // Left wall 1
    new Barrier(new Vector2(3950, -500), new Vector2(50, 2250)), // Right wall 2
    new Barrier(new Vector2(0, 1750), new Vector2(500, 250)), // bottom left 3
    new Barrier(new Vector2(3500, 1750), new Vector2(500, 250)), // bottom right 4
    new Barrier(new Vector2(1500, 1750), new Vector2(1000, 250)), // bottom middle 5
    new Barrier(new Vector2(750, 1350), new Vector2(500, 50)), // float bottom left 6
    new Barrier(new Vector2(2750, 1350), new Vector2(500, 50)), // float bottom right 7
    new Barrier(new Vector2(1500, 950), new Vector2(1000, 50)), // float center 8
    new Barrier(new Vector2(3150, 750), new Vector2(850, 50)), // right center 9
    new Barrier(new Vector2(0, 750), new Vector2(850, 50)), // left center 10
    new Barrier(new Vector2(1750, 500), new Vector2(500, 50)), // small center float 11
    new Barrier(new Vector2(2750, 300), new Vector2(500, 50)), // top right float 12
    new Barrier(new Vector2(750, 300), new Vector2(500, 50)), // top left float 13
    new Barrier(new Vector2(0, -500), new Vector2(4000, 50)), // Top 14
    new Barrier(new Vector2(1975, -500), new Vector2(50, 500)), // top center wall 15
    new Barrier(new Vector2(1975, 950), new Vector2(50, 400)), // bottom center wall 16
    new Barrier(new Vector2(2000, -500), new Vector2(2000, 2500), [false, false, false, false, false, false, false, false, false, false, true], [false], "#00000000"), // team a barrier
    new Barrier(new Vector2(0, -500), new Vector2(2000, 2500), [false, false, false, false, false, false, false, false, false, false, false, true], [false], "#00000000") // team b barrier
], [
    new Vector2(250, 1600),
    new Vector2(1000, 1200),
    new Vector2(400, 600),
    new Vector2(1000, 150)
], [
    new Vector2(3750, 1600),
    new Vector2(3000, 1200),
    new Vector2(3600, 600),
    new Vector2(3000, 150)
], [
    new Vector2(1875, 450),
    new Vector2(2000, 450),
    new Vector2(2125, 450),
    new Vector2(1675, 900),
    new Vector2(1875, 900),
    new Vector2(2125, 900),
    new Vector2(2375, 900),
    new Vector2(1675, 1700),
    new Vector2(1875, 1700),
    new Vector2(2125, 1700),
    new Vector2(2375, 1700),
], [
    new Nav(new Vector2(500, 1900), 1000, 0),
    new Nav(new Vector2(1500, 1650), 500, 5),
    new Nav(new Vector2(0, 1650), 500, 3),
    new Nav(new Vector2(750, 1250), 500, 6),
    new Nav(new Vector2(1500, 850), 500, 8),
    new Nav(new Vector2(0, 650), 850, 10),
    new Nav(new Vector2(1750, 400), 250, 14),
    new Nav(new Vector2(750, 200), 500, 13)
], [
    new Nav(new Vector2(2500, 1900), 1000, 0),
    new Nav(new Vector2(2000, 1650), 500, 5),
    new Nav(new Vector2(3500, 1650), 500, 4),
    new Nav(new Vector2(2750, 1250), 500, 7),
    new Nav(new Vector2(2000, 850), 500, 8),
    new Nav(new Vector2(3150, 650), 850, 9),
    new Nav(new Vector2(2000, 400), 250, 11),
    new Nav(new Vector2(2750, 200), 500, 12)
]);