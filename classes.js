function segmentIntersection(seg1A, seg1B, seg2A, seg2B) {
    let s1 = new Vector2(seg1B.x - seg1A.x, seg1B.y - seg1A.y);
    let s2 = new Vector2(seg2B.x - seg2A.x, seg2B.y - seg2A.y);

    let s = (-s1.y * (seg1A.x - seg2A.x) + s1.x * (seg1A.y - seg2A.y)) / (-s2.x * s1.y + s1.x * s2.y);
    let t = ( s2.x * (seg1A.y - seg2A.y) - s2.y * (seg1A.x - seg2A.x)) / (-s2.x * s1.y + s1.x * s2.y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        return new Vector2(seg1A.x + (t * s1.x), seg1A.y + (t * s1.y));
    }

    return null;
}

function clamp(val, min, max) {
    return Math.min(Math.max(val, min), max);
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const lerp = (number1, number2, whereBetween) => {
    var distanceBetween = number2 - number1;
    var offsetDistance = distanceBetween * whereBetween;
    return number1 + offsetDistance;
}

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex > 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
}

function sharesLayer(mask1, mask2) {
    for (let i = 0; i < mask1.length; i++) {
        if (mask1[i] && mask2[i]) {
            return true;
        }
    }

    return false;
}

function screenToWorld(screen) {
    let trans = ctx0.getTransform();
    return screen.subR(new Vector2(trans.e, trans.f)).divR(new Vector2(trans.a, trans.d));
}

function worldToScreen(world) {
    let trans = ctx0.getTransform();
    return world.mulR(new Vector2(trans.a, trans.d)).addR(new Vector2(trans.e, trans.f));
}

function pointInRect(point, position, size) {
    return (point.x > position.x && point.x < position.x + size.x && point.y > position.y && point.y < position.y + size.y);
}

class Vector2 {
    constructor (x, y = x) {
        this.x = x;
        this.y = y;
    }

    get copy () {
        return new Vector2(this.x, this.y);
    }

    get length () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    get normalized () {
        return this.divR(new Vector2(this.length));
    }

    set (vector2) {
        this.x = vector2.x;
        this.y = vector2.y;
    }

    addS (vector2) {
        this.x += vector2.x;
        this.y += vector2.y;
    }

    addR (vector2) {
        return new Vector2(this.x + vector2.x, this.y + vector2.y);
    }

    subS (vector2) {
        this.x -= vector2.x;
        this.y -= vector2.y;
    }

    subR (vector2) {
        return new Vector2(this.x - vector2.x, this.y - vector2.y);
    }

    mulS (vector2) {
        this.x *= vector2.x;
        this.y *= vector2.y;
    }

    mulR (vector2) {
        return new Vector2(this.x * vector2.x, this.y * vector2.y);
    }

    divS (vector2) {
        this.x /= vector2.x;
        this.y /= vector2.y;
    }

    divR (vector2) {
        return new Vector2(this.x / vector2.x, this.y / vector2.y);
    }

    dot (vector2) {
        return this.x * vector2.x + this.y * vector2.y;
    }

    notDot (vector2) {
        return this.x * vector2.y - this.y * vector2.x;
    }

    powS (power) { 
        this.x = Math.pow(this.x, power);
        this.y = Math.pow(this.y, power);
    }

    powR (power) {
        return new Vector2(Math.pow(this.x, power), Math.pow(this.y, power));
    }

    rotateS (angle) {
        this.x = this.x*Math.cos(angle) - this.y * Math.sin(angle);
        this.x = this.x*Math.sin(angle) + this.y * Math.cos(angle);
    }

    rotateR (angle) {
        let x = this.x*Math.cos(angle) - this.y * Math.sin(angle);
        let y = this.x*Math.sin(angle) + this.y * Math.cos(angle);
        return new Vector2(x, y);
    }

    angleTo (vector2) {
        let dot = this.normalized.dot(vector2.normalized);
        let dot2 = this.normalized.rotateR(Math.PI / 2).dot(vector2.normalized);
        return Math.acos(dot / 1) * (Math.floor(dot2 + 1) - 1);
    }

    simgpleAngleTo (vector2) {
        let dot = this.normalized.dot(vector2.normalized);
        let dot2 = this.normalized.notDot(vector2.normalized);
        return Math.atan2(dot2, dot);
    }

    distanceTo (vector2) {
        return this.subR(vector2).length;
    }

    lerp (vector2, value) {
        var distanceBetween = vector2.subR(this);
        var offsetDistance = distanceBetween.mulR(new Vector2(value));
        return this.addR(offsetDistance);
    }

    toString () {
        return "Vector2(" + this.x.toFixed(2) + ", " + this.y.toFixed(2) + ")";
    }
}

class Barrier {
    constructor (position, size, layer = [true], mask = [true], specialColor = false) {
        this.position = position;
        this.size = size;
        this.margin = 0.001;
        this.layer = layer;
        this.mask = mask;
        this.friction = 0.8;
        this.type = 0;
        this.specialColor = specialColor;
    }

    get area () {
        return this.size.x * this.size.y;
    }

    get max () {
        return this.position.addR(this.size);
    }

    get center () {
        return this.position.addR(this.size.divR(new Vector2(2)));
    }
}

class Circle {
    constructor (position, radius, mass, restitution = 0.2, layer = [true], mask = [true], pinned = false) {
        this.position = position;
        this.rotation = 0;
        this.radius = radius;
        this.mass = mass;
        this.velocity = new Vector2(0);
        this.angularVelocity = 0;
        this.restitution = restitution;
        this.friction = 0.9;
        this.links = [];
        this.linkLookup = [];
        this.pinned = pinned;
        this.type = 1;
        this.owner = null;

        this.lastPosition = position;

        this.layer = layer;
        this.mask = mask;

        this.onCollide = (object, type) => {};

        circles.push(this);
    }

    move (dT) {
        if (this.pinned) {
            this.velocity = new Vector2(0);
            this.position = this.lastPosition;

            return;
        }

        this.lastPosition = this.position;
        this.position.addS(this.velocity.mulR(new Vector2(dT)));
        this.rotation += this.angularVelocity * dT;
    }

    collideWithCircle (circle) {
        if (this.linkLookup.indexOf(circle) != -1) {
            this.linkedCollision(circle, this.links[this.linkLookup.indexOf(circle)]);
        }
        if ((!sharesLayer(this.layer, circle.mask) && !sharesLayer(this.mask, circle.layer)) || this.position.distanceTo(circle.position) >  this.radius + circle.radius) return;

        this.onCollide(circle, circle.type);
        circle.onCollide(this, this.type);
        
        let offset = this.position.subR(circle.position);
        let relativeVelocity = this.velocity.subR(circle.velocity);
        let impulse = relativeVelocity.dot(offset.normalized) * this.mass * circle.mass;

        let thisForce = offset.normalized.mulR(new Vector2(((impulse * (1 + this.restitution)) / (this.mass + circle.mass)) / this.mass));
        let circleForce = offset.normalized.mulR(new Vector2(((impulse * (1 + circle.restitution)) / (this.mass + circle.mass)) / circle.mass));
        let totalForce = thisForce.length + circleForce.length;
        if (isNaN(totalForce)) return;

        this.velocity.subS(thisForce);
        circle.velocity.addS(circleForce);

        /*let angle = relativeVelocity.simgpleAngleTo(offset.normalized);

        let torque = this.velocity.length * this.radius * Math.sin(angle);
        this.angularVelocity = torque / (0.5 * this.mass * Math.pow(this.radius, 2)) * ((this.friction + circle.friction) / 2);
        circle.angularVelocity = -torque / (0.5 * this.mass * Math.pow(this.radius, 2)) * ((this.friction + circle.friction) / 2);
        */

        if (totalForce == 0) return;

        let intersection = offset.length - (this.radius + circle.radius);
        
        if (this.pinned) {
            circle.position.addS(offset.normalized.mulR(new Vector2(intersection + 0.001)));
        } else if (circle.pinned) {
            this.position.subS(offset.normalized.mulR(new Vector2(intersection + 0.001)));
        } else {
            this.position.subS(offset.normalized.mulR(new Vector2(intersection * (thisForce.length / totalForce) + 0.001)));
            circle.position.addS(offset.normalized.mulR(new Vector2(intersection * (circleForce.length / totalForce) + 0.001)));
        }
    }

    linkedCollision (circle, link) {
        if (!link.enabled) return;
        
        if (link.type == 1) {
            let movingCircle = circle.pinned ? this : circle;
            let pinnedCircle = circle.pinned ? circle : this;

            movingCircle.position = pinnedCircle.position.copy;
            movingCircle.velocity = pinnedCircle.position.subR(movingCircle.position);
            return;
        }

        let distance = this.position.distanceTo(circle.position);
        let positionOffset = this.position.subR(circle.position);

        if (distance > link.maxDistance) {
            let offset = positionOffset.mulR(new Vector2(distance - link.maxDistance));
            let relativeVelocity = this.velocity.subR(circle.velocity);
            let force = -link.strength * offset.length - link.damping * relativeVelocity.dot(offset.normalized)

            this.velocity.addS(offset.normalized.mulR(new Vector2(force)));
            circle.velocity.subS(offset.normalized.mulR(new Vector2(force)));
        } else if (distance < link.minDistance) {
            let offset = positionOffset.mulR(new Vector2(distance - link.minDistance));
            let relativeVelocity = this.velocity.subR(circle.velocity);
            let force = -link.strength * offset.length - link.damping * relativeVelocity.dot(offset.normalized)

            this.velocity.addS(offset.normalized.mulR(new Vector2(force)));
            circle.velocity.subS(offset.normalized.mulR(new Vector2(force)));
        }
    }

    collideWithBarriers (barriers) {
        for (let i = 0; i < barriers.length; i++) {
            let barrier = barriers[i];
            
            if (!sharesLayer(this.layer, barrier.mask) && !sharesLayer(this.mask, barrier.layer)) continue;

            if (!(this.position.x + this.radius > barrier.position.x && this.position.x - this.radius < barrier.max.x &&
                this.position.y + this.radius > barrier.position.y && this.position.y - this.radius < barrier.max.y)) {
                continue;
            }

            let intersectionLeft = segmentIntersection(barrier.position.subR(new Vector2(0, this.radius)), new Vector2(barrier.position.x, barrier.max.y + this.radius), this.position.subR(new Vector2(this.radius, 0)), this.position.addR(new Vector2(this.radius, 0)));
            let intersectionRight = segmentIntersection(barrier.max.addR(new Vector2(0, this.radius)), new Vector2(barrier.max.x, barrier.position.y - this.radius), this.position.addR(new Vector2(this.radius, 0)), this.position.subR(new Vector2(this.radius, 0)));
            
            let intersectionTop = segmentIntersection(barrier.position.subR(new Vector2(this.radius, 0)), new Vector2(barrier.max.x + this.radius, barrier.position.y), this.position.subR(new Vector2(0, this.radius)), this.position.addR(new Vector2(0, this.radius)));
            let intersectionBottom = segmentIntersection(barrier.max.addR(new Vector2(this.radius, 0)), new Vector2(barrier.position.x - this.radius, barrier.max.y), this.position.addR(new Vector2(0, this.radius)), this.position.subR(new Vector2(0, this.radius)));

            let intersection;

            if (intersectionLeft != null) intersection = new Vector2(clamp(intersectionLeft.x, barrier.position.x, barrier.max.x), clamp(intersectionLeft.y, barrier.position.y, barrier.max.y));
            if (intersectionRight != null) intersection = new Vector2(clamp(intersectionRight.x, barrier.position.x, barrier.max.x), clamp(intersectionRight.y, barrier.position.y, barrier.max.y));
            if (intersectionTop != null) intersection = new Vector2(clamp(intersectionTop.x, barrier.position.x, barrier.max.x), clamp(intersectionTop.y, barrier.position.y, barrier.max.y));
            if (intersectionBottom != null) intersection = new Vector2(clamp(intersectionBottom.x, barrier.position.x, barrier.max.x), clamp(intersectionBottom.y, barrier.position.y, barrier.max.y));

            if (intersection == null || this.position.distanceTo(intersection) > this.radius) continue;

            playPipe();
            this.onCollide(barrier, barrier.type);

            let collisionNormal = (this.lastPosition.subR(intersection)).normalized;

            let relativeVelocity = this.velocity.subR(new Vector2(0));
            let impulse = relativeVelocity.dot(collisionNormal) * this.mass * 99999999;

            let thisForce = collisionNormal.mulR(new Vector2(((impulse * (1 + this.restitution)) / (this.mass + 99999999)) / this.mass));
            if (isNaN(thisForce.length)) return;

            this.velocity.subS(thisForce);

            let collisionOffset = this.position.distanceTo(intersection) - this.radius;
            this.position.subS(collisionNormal.mulR(new Vector2(collisionOffset - 1)));
        }
    }
}

class Link {
    constructor (circle1, circle2, type, maxDistance = circle1.radius + circle2.radius, minDistance = 0, angle = 0, strength = 0.1, damping = 0.9) {
        this.circle1 = circle1;
        this.circle2 = circle2;
        this.type = type;
        this.minDistance = minDistance;
        this.maxDistance = maxDistance;
        this.angle = angle;
        this.strength = strength;
        this.damping = damping;
        this.enabled = true;

        circle1.links.push(this);
        circle1.linkLookup.push(circle2);
        circle2.links.push(this);
        circle2.linkLookup.push(circle1);
    }

    setEnabled (enabled) {
        this.enabled = enabled;
    }

    delete () {
        this.circle1.links.splice(this.circle1.links.indexOf(this), 1);
        this.circle2.links.splice(this.circle2.links.indexOf(this), 1);
        this.circle1.linkLookup.splice(this.circle1.linkLookup.indexOf(this.circle2), 1);
        this.circle2.linkLookup.splice(this.circle2.linkLookup.indexOf(this.circle1), 1);
    }
}

class Person {
    constructor (position, team = 0, human = false) {
        this.parts = [];
        this.bodyLinks = [];
        this.pinCircles = [];
        this.pins = [];

        this.idle = [];
        this.crouch = [];
        this.currentAnim = this.idle;

        this.foregroundHand = new Circle(position.addR(new Vector2(0, 22.5)), 15, 2, 0.2, [], [true]);
        this.foregroundElbow = new Circle(position.subR(new Vector2(0, 12.5)), 17.5, 2, 0.2, [], [true]);
        this.foregroundFoot = new Circle(position.addR(new Vector2(0, 85)), 20, 2, 0.2, [], [true]);
        this.foregroundKnee = new Circle(position.addR(new Vector2(0, 45)), 20, 2, 0.2, [], [true]);
        this.head = new Circle(position.subR(new Vector2(0, 97.5)), 22.5, 1, 0.2, [], [true]);
        this.chest = new Circle(position.subR(new Vector2(0, 50)), 25, 2, 0.2, [], [true]);
        this.waist = new Circle(position.copy, 25, 2, 0.5, [], [true]);
        this.backgroundKnee = new Circle(position.addR(new Vector2(0, 45)), 20, 2, 0.2, [], [true]);
        this.backgroundFoot = new Circle(position.addR(new Vector2(0, 85)), 20, 2, 0.2, [], [true]);
        this.backgroundElbow = new Circle(position.subR(new Vector2(0, 12.5)), 17.5, 2, 0.2, [], [true]);
        this.backgroundHand = new Circle(position.addR(new Vector2(0, 22.5)), 15, 2, 0.2, [], [true]);

        this.foregroundHand.owner = this;
        this.foregroundElbow.owner = this;
        this.foregroundFoot.owner = this;
        this.foregroundKnee.owner = this;
        this.head.owner = this;
        this.chest.owner = this;
        this.waist.owner = this;
        this.backgroundKnee.owner = this;
        this.backgroundFoot.owner = this;
        this.backgroundElbow.owner = this;
        this.backgroundHand.owner = this;

        this.foregroundHand.type = team == 0 ? 4 : 5;
        this.foregroundElbow.type = team == 0 ? 4 : 5;
        this.foregroundFoot.type = team == 0 ? 4 : 5;
        this.foregroundKnee.type = team == 0 ? 4 : 5;
        this.head.type = team == 0 ? 4 : 5;
        this.chest.type = team == 0 ? 4 : 5;
        this.waist.type = team == 0 ? 4 : 5;
        this.backgroundKnee.type = team == 0 ? 4 : 5;
        this.backgroundFoot.type = team == 0 ? 4 : 5;
        this.backgroundElbow.type = team == 0 ? 4 : 5;
        this.backgroundHand.type = team == 0 ? 4 : 5;

        if (team == 0) {
            this.foregroundHand.mask[10] = true;
            this.foregroundElbow.mask[10] = true;
            this.foregroundFoot.mask[10] = true;
            this.foregroundKnee.mask[10] = true;
            this.head.mask[10] = true;
            this.head.mask[10] = true;
            this.chest.mask[10] = true;
            this.chest.mask[10] = true;
            this.waist.mask[10] = true;
            this.waist.mask[10] = true;
            this.backgroundKnee.mask[10] = true;
            this.backgroundFoot.mask[10] = true;
            this.backgroundElbow.mask[10] = true;
            this.backgroundHand.mask[10] = true;
        } else if (team == 1) {
            this.foregroundHand.mask[11] = true;
            this.foregroundElbow.mask[11] = true;
            this.foregroundFoot.mask[11] = true;
            this.foregroundKnee.mask[11] = true;
            this.head.mask[11] = true;
            this.head.mask[11] = true;
            this.chest.mask[11] = true;
            this.chest.mask[11] = true;
            this.waist.mask[11] = true;
            this.waist.mask[11] = true;
            this.backgroundKnee.mask[11] = true;
            this.backgroundFoot.mask[11] = true;
            this.backgroundElbow.mask[11] = true;
            this.backgroundHand.mask[11] = true;
        }
        
        this.foregroundHandElbow = new Link(this.foregroundHand, this.foregroundElbow, 0, this.foregroundHand.radius + this.foregroundElbow.radius, this.foregroundHand.radius + this.foregroundElbow.radius);
        this.foregroundElbowChest = new Link(this.foregroundElbow, this.chest, 0, this.foregroundElbow.radius + 20, this.foregroundElbow.radius + 20);
        this.foregroundFootKnee = new Link(this.foregroundFoot, this.foregroundKnee, 0, this.foregroundFoot.radius + this.foregroundKnee.radius, this.foregroundFoot.radius + this.foregroundKnee.radius);
        this.foregroundKneeWaist = new Link(this.foregroundKnee, this.waist, 0, this.foregroundKnee.radius + this.waist.radius, this.foregroundKnee.radius + this.waist.radius);
        this.headChest = new Link(this.head, this.chest, 0, this.head.radius + this.chest.radius, this.head.radius + this.chest.radius);
        this.chestWaist = new Link(this.chest, this.waist, 0, this.chest.radius + this.waist.radius, this.chest.radius + this.waist.radius);
        this.backgroundKneeWaist = new Link(this.backgroundKnee, this.waist, 0, this.backgroundKnee.radius + this.waist.radius, this.backgroundKnee.radius + this.waist.radius);
        this.backgroundFootKnee = new Link(this.backgroundFoot, this.backgroundKnee, 0, this.backgroundFoot.radius + this.backgroundKnee.radius, this.backgroundFoot.radius + this.backgroundKnee.radius);
        this.backgroundElbowChest = new Link(this.backgroundElbow, this.chest, 0, this.backgroundElbow.radius + 20, this.backgroundElbow.radius + 20);
        this.backgroundHandElbow = new Link(this.backgroundHand, this.backgroundElbow, 0, this.backgroundHand.radius + this.backgroundElbow.radius, this.backgroundHand.radius + this.backgroundElbow.radius);
    
        this.parts.push(this.foregroundHand);
        this.parts.push(this.foregroundElbow);
        this.parts.push(this.foregroundFoot);
        this.parts.push(this.foregroundKnee);
        this.parts.push(this.head);
        this.parts.push(this.chest);
        this.parts.push(this.waist);
        this.parts.push(this.backgroundKnee);
        this.parts.push(this.backgroundFoot);
        this.parts.push(this.backgroundElbow);
        this.parts.push(this.backgroundHand);

        this.bodyLinks.push(this.foregroundHandElbow);
        this.bodyLinks.push(this.foregroundElbowChest);
        this.bodyLinks.push(this.foregroundFootKnee);
        this.bodyLinks.push(this.foregroundKneeWaist);
        this.bodyLinks.push(this.headChest);
        this.bodyLinks.push(this.chestWaist);
        this.bodyLinks.push(this.backgroundKneeWaist);
        this.bodyLinks.push(this.backgroundFootKnee);
        this.bodyLinks.push(this.backgroundElbowChest);
        this.bodyLinks.push(this.backgroundHandElbow);

        this.headPinCircle = new Circle(position.subR(new Vector2(0, 150)), 0, 0, 0, [], [], true);
        this.chestPinCircle = new Circle(position.subR(new Vector2(0, 50)), 0, 0, 0, [], [], true);
        this.waistPinCircle = new Circle(position.copy, 0, 0, 0, [], [], true);
        this.foregroundHandPinCircle = new Circle(position.subR(new Vector2(0, -30)), 0, 0, 0, [], [], true);
        this.backgroundHandPinCircle = new Circle(position.subR(new Vector2(0, -30)), 0, 0, 0, [], [], true);
        this.foregroundFootPinCircle = new Circle(position.subR(new Vector2(0, -90)), 0, 0, 0, [], [], true);
        this.backgroundFootPinCircle = new Circle(position.subR(new Vector2(0, -90)), 0, 0, 0, [], [], true);

        this.pinStrength = 0.3;
        this.pinDamping = 1;
        this.bodyStrength = 0.1;
        this.bodyDamping = 0.9;

        this.headPin = new Link(this.headPinCircle, this.head, 0, 55, 0, 0, this.pinStrength, this.pinDamping);
        this.chestPin = new Link(this.chestPinCircle, this.chest, 0, 0, 0, 0, this.pinStrength, this.pinDamping);
        this.waistPin = new Link(this.waistPinCircle, this.waist, 0, 0, 0, 0, this.pinStrength, this.pinDamping);
        this.foregroundHandPin = new Link(this.foregroundHandPinCircle, this.foregroundHand, 0, 0, 0, 0, this.pinStrength, this.pinDamping);
        this.backgroundHandPin = new Link(this.backgroundHandPinCircle, this.backgroundHand, 0, 0, 0, 0, this.pinStrength, this.pinDamping);
        this.foregroundFootPin = new Link(this.foregroundFootPinCircle, this.foregroundFoot, 0, 0, 0, 0, this.pinStrength, this.pinDamping);
        this.backgroundFootPin = new Link(this.backgroundFootPinCircle, this.backgroundFoot, 0, 0, 0, 0, this.pinStrength, this.pinDamping);

        this.pinCircles.push(this.headPinCircle);
        this.pinCircles.push(this.chestPinCircle);
        this.pinCircles.push(this.waistPinCircle);
        this.pinCircles.push(this.foregroundHandPinCircle);
        this.pinCircles.push(this.backgroundHandPinCircle);
        this.pinCircles.push(this.foregroundFootPinCircle);
        this.pinCircles.push(this.backgroundFootPinCircle);

        this.pins.push(this.headPin);
        this.pins.push(this.chestPin);
        this.pins.push(this.waistPin);
        this.pins.push(this.foregroundHandPin);
        this.pins.push(this.backgroundHandPin);
        this.pins.push(this.foregroundFootPin);
        this.pins.push(this.backgroundFootPin);

        this.idle.push(new Vector2(0, 150));
        this.idle.push(new Vector2(0, 50));
        this.idle.push(new Vector2(0));
        this.idle.push(new Vector2(0, 50));
        this.idle.push(new Vector2(0, 50));
        this.idle.push(new Vector2(0, -100));
        this.idle.push(new Vector2(0, -100));

        this.crouch.push(new Vector2(0, 150));
        this.crouch.push(new Vector2(0, 50));
        this.crouch.push(new Vector2(0));
        this.crouch.push(new Vector2(0, 50));
        this.crouch.push(new Vector2(0, 50));
        this.crouch.push(new Vector2(0, -50));
        this.crouch.push(new Vector2(0, -50));

        this.team = team;
        this.setTeam(this.team);

        this.armLength = 70;
        this.human = human;
        this.aim = new Vector2(canvas0.width / 2, canvas0.height / 2);
        this.velocity = new Vector2(0);
        this.movementKeys = {"w": 0, "a": 0, "s": 0, "d": 0, "j": 0};
        this.crouched = false;
        this.armAction = 0;
        this.indiArmAction = [false, false, false];
        this.lastArmAction = 0;
        this.rightTop = new Vector2(50, -120);
        this.leftBottom = new Vector2(-50, 110);
        this.position = position.copy;
        this.lastPosition = position.copy;

        this.getUpTime = 0.5;
        this.gettingUp = -1;
        this.getUpStart = [];
        this.ragdolled = false;
        this.grounded = false;

        this.heldBall = null;
        this.ballHoldLink = null;

        this.runTime = 0;
        this.moving = 0;

        this.throwDelay = 0.1;
        this.throwTimer = -1;

        this.throwCharge = 0;

        this.focusPoint = this.chestPinCircle;

        this.ballPickup = new Circle(new Vector2(0), 35, 0.1, 0, [], [], true);
        this.ballPickup.type = 2;
        this.ballPickup.onCollide = this.pickupBall.bind(this);

        this.jailed = false;
        this.canGetUp = true;

        this.iframeTimer = -1;
        this.iframeLength = 5;

        this.playerCloserTime = 0;
        this.ballCloserTimer = 0;
        this.ignoreBall = null;

        this.ragdollTimer = -1;
        this.jumpForce = jumpForce;
        this.moveSpeed = moveSpeed;
        this.landedOnLastNav = true;
        this.nearestBall;
        this.targetNav;
        this.nearestNav;
        this.nearestPlayer;
        this.aimKeys = {"u": 0, "d": 0, "l": 0, "r": 0};
    }

    aimAt (location) {
        this.aim = location;
    }

    pickupBall (thing, type) {
        if (type == 3) {
            this.heldBall = thing.owner;
            this.heldBall.setTeam(this.team);
            this.ballHoldLink = new Link(this.ballPickup, this.heldBall.circle, 1, 0, 0, 0, 1, 1);
            this.ballPickup.layer[1] = false;
            this.ballPickup.layer[2] = false;
            this.ballPickup.layer[3] = false;

            this.heldBall.circle.mask[0] = false;
        }
    }

    teleport (position) {
        let offset = position.subR(this.position);

        for (let i = 0; i < this.parts.length; i++) {
            this.parts[i].position.addS(offset);
        }

        this.position = position.copy;
        for (let i = 0; i < this.pinCircles.length; i++) {
            this.pinCircles[i].position.set(this.position.subR(this.currentAnim[i]));
        }
        
    }

    move (dT) {
        if (!this.human) this.doAIStuff(dT);

        this.armAction = (this.indiArmAction[0] ? 1 : 0) + (this.indiArmAction[2] ? -1 : 0);
        if (this.ragdolled) this.armAction = 0;

        if (!this.ragdolled) {
            this.ragdollTimer = 0;

            if (this.gettingUp != -1) {
                this.gettingUp += dT;

                this.bodyDamping = lerp(1, 0.9, this.gettingUp / this.getUpTime);

                for (let i = 0; i < this.pinCircles.length; i++) {
                    if (this.idle[i] === false) {
                        this.pins[i].setEnabled(false);
                    } else {
                        this.pinCircles[i].position.set(this.getUpStart[i].lerp(this.position.subR(this.idle[i]), this.gettingUp / this.getUpTime));
                    }
                }

                if (this.gettingUp >= this.getUpTime) {
                    this.focusPoint = this.chestPinCircle;
                    this.gettingUp = -1;

                    this.bodyDamping = 0.9;

                    this.bodyLinks.forEach(link => {
                        link.minDistance = link.maxDistance;
                    });
                }

                this.bodyLinks.forEach(link => {
                    link.damping = this.bodyDamping;
                });

                return;
            }

            if (this.iframeTimer != -1) {
                this.iframeTimer += dT;

                if (this.iframeTimer >= this.iframeLength) {
                    this.setTeam(this.team);
                    this.iframeTimer = -1;
                }
            }

            if (this.movementKeys.s == 1) {
                this.leftBottom.y = 60;
                this.crouched = true;
            } else {
                if (this.crouched) {this.lastPosition.y -= 50}
                this.leftBottom.y = 110;
                this.crouched = false;
            }

            this.velocity.x = this.movementKeys.a * -this.moveSpeed + this.movementKeys.d * this.moveSpeed;
            this.velocity.y = clamp(this.velocity.y + 1500 * dT * gravityMod, -100000, 1500);

            this.grounded = false;

            for (let i = 0; i < barriers.length; i++) {
                let barrier = barriers[i];

                if (barrier.layer[11] && this.team == 0) continue;
                if (barrier.layer[10] && this.team == 1) continue;

                if (this.position.x + this.rightTop.x > barrier.position.x && this.position.x + this.leftBottom.x < barrier.position.x + barrier.size.x && this.position.y + this.rightTop.y < barrier.position.y + barrier.size.y && this.position.y + this.leftBottom.y > barrier.position.y) {
                    let directionalIntersection = new Vector2(this.position.x, this.lastPosition.y);
                    if (directionalIntersection.x + this.rightTop.x > barrier.position.x && directionalIntersection.x + this.leftBottom.x < barrier.position.x + barrier.size.x && directionalIntersection.y + this.rightTop.y < barrier.position.y + barrier.size.y && directionalIntersection.y + this.leftBottom.y > barrier.position.y) {
                        this.velocity.x = 0;

                        if (this.position.x > barrier.position.x + barrier.size.x / 2) {
                            this.position.x = barrier.position.x + barrier.size.x - this.leftBottom.x;
                        } else {
                            this.position.x = barrier.position.x - this.rightTop.x;
                        }
                    }

                    directionalIntersection = new Vector2(this.lastPosition.x, this.position.y);
                    if (directionalIntersection.x + this.rightTop.x > barrier.position.x && directionalIntersection.x + this.leftBottom.x < barrier.position.x + barrier.size.x && directionalIntersection.y + this.rightTop.y < barrier.position.y + barrier.size.y && directionalIntersection.y + this.leftBottom.y > barrier.position.y) {
                        this.velocity.y = 0;

                        if (this.position.y > barrier.position.y + barrier.size.y / 2) {
                            this.position.y = barrier.position.y + barrier.size.y - this.rightTop.y;
                        } else {
                            this.position.y = barrier.position.y - this.leftBottom.y;
                            this.grounded = true;
                            /*if (this.targetNav != null && barrier == game.map.barriers[this.targetNav.barrier]) {
                                this.landedOnLastNav = true;
                            }*/
                        }
                    }
                }
            }

            if (this.grounded && this.movementKeys.j) {
                this.velocity.y = this.jumpForce;
                this.movementKeys.j = 0;
            }
        
            this.lastPosition = this.position.copy;
            this.position.addS(this.velocity.mulR(new Vector2(dT)));

            if (this.movementKeys.a) {
                this.runTime -= dT * 10;
                this.moving = clamp(this.moving + dT * 2, 0, 1);
            } else if (this.movementKeys.d) {
                this.runTime += dT * 10;
                this.moving = clamp(this.moving + dT * 2, 0, 1);
            } else {
                this.moving = clamp(this.moving - dT * 5, 0, 1);
            }

            if (this.crouched) {
                this.currentAnim = this.crouch;
            } else {
                this.currentAnim = this.idle;
            }

            for (let i = 0; i < this.pinCircles.length; i++) {
                this.pinCircles[i].position.set(this.position.subR(this.currentAnim[i]));
            }

            this.foregroundFootPinCircle.position.addS(new Vector2(Math.cos(this.runTime) * 40 * this.moving, clamp(Math.sin(this.runTime) * 25 * this.moving - 10, -100, 0)));
            this.backgroundFootPinCircle.position.addS(new Vector2(Math.cos(this.runTime - Math.PI) * 40 * this.moving, clamp(Math.sin(this.runTime - Math.PI) * 25 * this.moving - 10, -100, 0)));

            let armAngle = Math.sin(this.runTime) * Math.PI / 3;
            let armSwing = new Vector2(0, 1).rotateR(-armAngle).mulR(new Vector2(this.armLength));
            this.foregroundHandPinCircle.position.addS(new Vector2(0, this.armLength).lerp(armSwing, this.moving));
            this.backgroundHandPinCircle.position.addS(new Vector2(0, this.armLength).lerp(armSwing.mulR(new Vector2(-1, 1)), this.moving));

            if (this.throwTimer != -1) {
                let aimToCursor = screenToWorld(this.aim).subR(this.chest.position).normalized;
                this.foregroundHandPinCircle.position.set(this.position.subR(this.currentAnim[1]).addR(aimToCursor.mulR(new Vector2(this.armLength))));

                this.throwTimer += dT;

                if (this.throwTimer >= this.throwDelay) {
                    this.throwTimer = -1;
                }
            } if (this.armAction == 1 && this.heldBall != null) {
                let aimToCursor = screenToWorld(this.aim).subR(this.chest.position).normalized;
                this.foregroundHandPinCircle.position.set(this.position.subR(this.currentAnim[1]).subR(aimToCursor.mulR(new Vector2(this.armLength))));
                this.throwCharge = clamp(this.throwCharge + dT, 0, 1);
            } else if (this.armAction == -1) {
                let aimToCursor = screenToWorld(this.aim).subR(this.chest.position).normalized;
                this.foregroundHandPinCircle.position.set(this.position.subR(this.currentAnim[1]).addR(aimToCursor.mulR(new Vector2(this.armLength))));
                this.backgroundHandPinCircle.position.set(this.position.subR(this.currentAnim[1]).addR(aimToCursor.mulR(new Vector2(this.armLength))));
                this.throwCharge = 0;

                if (this.heldBall == null) {
                    this.ballPickup.layer[1] = true;
                    this.ballPickup.layer[2] = (this.team == 0);
                    this.ballPickup.layer[3] = (this.team == 1);
                }
            } else {
                if (this.heldBall != null && this.lastArmAction == 1 && !this.indiArmAction[0] && !this.indiArmAction[1] && !this.indiArmAction[2]) {
                    let aimToCursor = screenToWorld(this.aim).subR(this.chest.position).normalized;
                    this.heldBall.throw(this, aimToCursor.mulR(new Vector2(throwStrength * this.throwCharge)));
                    this.ballHoldLink.delete();
                    this.heldBall = null;
                    this.throwTimer = 0;
                }
                this.throwCharge = 0;

                this.ballPickup.layer[1] = false;
                this.ballPickup.layer[2] = false;
                this.ballPickup.layer[3] = false;
            }
            this.lastArmAction = this.armAction;

            this.ballPickup.position.set(this.foregroundHand.position.copy);
        } else {
            this.ragdollTimer += dT;

            this.position = this.waist.position.copy;

            this.grounded = false;

            for (let i = 0; i < barriers.length; i++) {
                let barrier = barriers[i];

                if (barrier.layer[11] && this.team == 0) continue;
                if (barrier.layer[10] && this.team == 1) continue;

                if (this.position.x + this.rightTop.x > barrier.position.x && this.position.x + this.leftBottom.x < barrier.position.x + barrier.size.x && this.position.y + this.rightTop.y < barrier.position.y + barrier.size.y && this.position.y + this.leftBottom.y > barrier.position.y) {
                    let directionalIntersection = new Vector2(this.position.x, this.lastPosition.y);
                    if (directionalIntersection.x + this.rightTop.x > barrier.position.x && directionalIntersection.x + this.leftBottom.x < barrier.position.x + barrier.size.x && directionalIntersection.y + this.rightTop.y < barrier.position.y + barrier.size.y && directionalIntersection.y + this.leftBottom.y > barrier.position.y) {
                        this.velocity.x = 0;

                        if (this.position.x > barrier.position.x + barrier.size.x / 2) {
                            this.position.x = barrier.position.x + barrier.size.x - this.leftBottom.x;
                        } else {
                            this.position.x = barrier.position.x - this.rightTop.x;
                        }
                    }

                    directionalIntersection = new Vector2(this.lastPosition.x, this.position.y);
                    if (directionalIntersection.x + this.rightTop.x > barrier.position.x && directionalIntersection.x + this.leftBottom.x < barrier.position.x + barrier.size.x && directionalIntersection.y + this.rightTop.y < barrier.position.y + barrier.size.y && directionalIntersection.y + this.leftBottom.y > barrier.position.y) {
                        this.velocity.y = 0;

                        if (this.position.y > barrier.position.y + barrier.size.y / 2) {
                            this.position.y = barrier.position.y + barrier.size.y - this.rightTop.y;
                        } else {
                            this.position.y = barrier.position.y - this.leftBottom.y;
                            this.grounded = true;
                        }
                    }
                }
            }

            this.lastPosition = this.position.copy;
            for (let i = 0; i < this.pinCircles.length; i++) {
                this.pinCircles[i].position.set(this.position.subR(this.currentAnim[i]));
            }
        }
    }

    doAIStuff (dT) {
        if (this.ragdollTimer > 2) {
            this.ragdoll(false);
        }

        if (this.heldBall == null) {
            this.nearestBall = game.dodgeballs[0];
            if (this.nearestBall == this.ignoreBall) {game.dodgeballs[1]};

            for (let i = game.dodgeballs.indexOf(this.nearestBall) + 1; i < game.dodgeballs.length; i++) {
                if (game.dodgeballs[i].circle.position.distanceTo(this.position) < this.nearestBall.circle.position.distanceTo(this.position) && game.dodgeballs[i] != this.ignoreBall
                && (game.dodgeballs[i].circle.position.x >= game.map.center && this.team == 1 || game.dodgeballs[i].circle.position.x <= game.map.center && this.team == 0) && game.dodgeballs[i].team != this.team) {
                    this.nearestBall = game.dodgeballs[i];
                }
            }

            let nav = this.team == 0 ? game.map.teamANav : game.map.teamBNav;

            this.targetNav = new Nav(new Vector2(9999999999, 9999999999), 0);
            this.nearestNav = new Nav(new Vector2(9999999999, 9999999999), 0);
            let closestPointNav = this.nearestNav.center;
            let targetClosestPointNav = this.targetNav.center;
            let closestonNearNav = this.nearestNav.center;

            if (this.nearestBall.circle.position.x < game.map.center && this.team == 1) {
                this.nearestBall = {"circle": {"position": new Vector2(100, 0)}}
            }

            if (this.nearestBall.circle.position.x > game.map.center && this.team == 0) {
                this.nearestBall = {"circle": {"position": new Vector2(3900, 0)}}
            }

            for (let i = 0; i < nav.length; i++) {
                if (nav[i].center.distanceTo(this.nearestBall.circle.position) < this.targetNav.center.distanceTo(this.nearestBall.circle.position) && this.position.y - nav[i].position.y < 500) {
                    this.targetNav = nav[i];
                }

                if (nav[i].center.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].center;
                } 
                if (nav[i].position.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].position;
                } 
                if (nav[i].position2.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].position2;
                }
            }

            if (this.targetNav.center.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                closestPointNav = this.targetNav.center;
            } 
            if (this.targetNav.position.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                targetClosestPointNav = this.targetNav.position;
            } 
            if (this.targetNav.position2.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                targetClosestPointNav = this.targetNav.position2;
            }

            if (this.nearestNav.center.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.center;
            } 
            if (this.nearestNav.position.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.position;
            } 
            if (this.nearestNav.position2.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.position2;
            }

            let navOffset = this.targetNav.center.subR(this.position);
            let ballOffset = this.nearestBall.circle.position.subR(this.position);
            let timeToLand = (-this.jumpForce + Math.sqrt(this.jumpForce*this.jumpForce - 4 * 0.5 * 1500 * (this.nearestNav.position.y - this.targetNav.position.y))) / 1500;
            let timeToFall = (0 + Math.sqrt(0 - 4 * 0.5 * 1500 * (this.nearestNav.position.y - this.targetNav.position.y))) / 1500;

            if (this.nearestNav != this.targetNav) {
                if (this.grounded) {
                    if (navOffset.x < 0) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else if (navOffset.x > 0) {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 0;
                    }   
    
                    if (this.grounded && navOffset.y < 0 && Math.abs(navOffset.x) / this.moveSpeed <= timeToLand
                    || this.grounded && Math.abs(closestonNearNav.x - targetClosestPointNav.x) / this.moveSpeed > timeToFall) {
                        this.movementKeys.j = 1;
                    }
                } else if (Math.abs(navOffset.x) / this.moveSpeed >= timeToLand) {
                    if (navOffset.x < 0) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else if (navOffset.x > 0) {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 0;
                    }
                }

                if ((this.nearestNav.center.x > this.targetNav.position.x && this.nearestNav.center.x < this.targetNav.position2.x || this.targetNav.center.x > this.nearestNav.position.x && this.targetNav.center.x < this.nearestNav.position2.x) && this.targetNav.position.y > this.nearestNav.position.y) {
                    if (this.position.x < this.nearestNav.center.x && this.nearestNav.position.x != 2000 && this.nearestNav.position.x != 0 && this.nearestNav.position2.x != 4000 && this.nearestNav.position2.x != 2000) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    }
                }
            } else {
                this.movementKeys.j = 0;

                if (ballOffset.x < 0) {
                    this.movementKeys.a = 1;
                    this.movementKeys.d = 0;
                } else if (ballOffset.x > 0) {
                    this.movementKeys.a = 0;
                    this.movementKeys.d = 1;
                } else {
                    this.movementKeys.a = 0;
                    this.movementKeys.d = 0;
                }

                if (ballOffset.length < 250) {
                    this.aimAt(worldToScreen(this.nearestBall.circle.position));
                    this.indiArmAction[2] = true;
                    this.movementKeys.s = 1;
                } else {
                    this.indiArmAction[2] = false;
                }
            }
        } else {
            this.movementKeys.s = 0;

            for (let i = 0; i < game.players.length; i++) {
                if (this.nearestPlayer == null && game.players[i].team != this.team || this.nearestPlayer != null && game.players[i].position.distanceTo(this.position) < this.nearestPlayer.position.distanceTo(this.position) && game.players[i] != this && game.players[i].team != this.team) {
                    this.nearestPlayer = game.players[i];
                }
            }

            let nav = this.team == 0 ? game.map.teamANav : game.map.teamBNav;

            this.targetNav = new Nav(new Vector2(9999999999, 9999999999), 0);
            this.nearestNav = new Nav(new Vector2(9999999999, 9999999999), 0);
            let closestPointNav = this.nearestNav.center;
            let targetClosestPointNav = this.targetNav.center;
            let closestonNearNav = this.nearestNav.center;

            for (let i = 0; i < nav.length; i++) {
                if (nav[i].center.distanceTo(this.nearestPlayer.position) < this.targetNav.center.distanceTo(this.nearestPlayer.position) && this.position.y - nav[i].position.y < 500) {
                    this.targetNav = nav[i];
                }

                if (nav[i].center.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].center;
                } 
                if (nav[i].position.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].position;
                } 
                if (nav[i].position2.distanceTo(this.position) < closestPointNav.distanceTo(this.position)) {
                    this.nearestNav = nav[i];
                    closestPointNav = nav[i].position2;
                }
            }

            if (this.targetNav.center.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                closestPointNav = this.targetNav.center;
            } 
            if (this.targetNav.position.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                targetClosestPointNav = this.targetNav.position;
            } 
            if (this.targetNav.position2.distanceTo(this.position) < targetClosestPointNav.distanceTo(this.position)) {
                targetClosestPointNav = this.targetNav.position2;
            }

            if (this.nearestNav.center.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.center;
            } 
            if (this.nearestNav.position.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.position;
            } 
            if (this.nearestNav.position2.distanceTo(targetClosestPointNav) < closestonNearNav.distanceTo(targetClosestPointNav)) {
                closestonNearNav = this.nearestNav.position2;
            }

            let navOffset = this.targetNav.center.subR(this.position);
            let playerOffset = this.nearestPlayer.position.subR(this.position);
            let timeToLand = (-this.jumpForce + Math.sqrt(this.jumpForce*this.jumpForce - 4 * 0.5 * 1500 * (this.nearestNav.position.y - this.targetNav.position.y))) / 1500;
            let timeToFall = (0 + Math.sqrt(0 - 4 * 0.5 * 1500 * (this.nearestNav.position.y - this.targetNav.position.y))) / 1500;

            if (this.nearestNav != this.targetNav) {
                if (this.grounded) {
                    if (navOffset.x < 0) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else if (navOffset.x > 0) {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 0;
                    }   
    
                    if (this.grounded && navOffset.y < 0 && Math.abs(navOffset.x) / this.moveSpeed <= timeToLand
                    || this.grounded && Math.abs(closestonNearNav.x - targetClosestPointNav.x) / this.moveSpeed > timeToFall) {
                        this.movementKeys.j = 1;
                    }
                } else if (Math.abs(navOffset.x) / this.moveSpeed >= timeToLand) {
                    if (navOffset.x < 0) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else if (navOffset.x > 0) {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 0;
                    }
                }

                if ((this.nearestNav.center.x > this.targetNav.position.x && this.nearestNav.center.x < this.targetNav.position2.x || this.targetNav.center.x > this.nearestNav.position.x && this.targetNav.center.x < this.nearestNav.position2.x) && this.targetNav.position.y > this.nearestNav.position.y) {
                    if (this.position.x < this.nearestNav.center.x && this.nearestNav.position.x != 2000 && this.nearestNav.position.x != 0 && this.nearestNav.position2.x != 4000 && this.nearestNav.position2.x != 2000) {
                        this.movementKeys.a = 1;
                        this.movementKeys.d = 0;
                    } else {
                        this.movementKeys.a = 0;
                        this.movementKeys.d = 1;
                    }
                }
            } else {
                this.movementKeys.j = 0;

                if (this.nearestPlayer.x < 0) {
                    this.movementKeys.a = 1;
                    this.movementKeys.d = 0;
                } else if (this.nearestPlayer.x > 0) {
                    this.movementKeys.a = 0;
                    this.movementKeys.d = 1;
                } else {
                    this.movementKeys.a = 0;
                    this.movementKeys.d = 0;
                }   
            }

            if (!this.indiArmAction[0]) {
                this.aimAt(worldToScreen(this.nearestPlayer.position));
                this.indiArmAction[2] = true;
            }

            if (this.position.distanceTo(this.nearestPlayer.position) < 1000) {
                this.aimAt(worldToScreen(this.nearestPlayer.position.addR(new Vector2(0, -this.position.distanceTo(this.nearestPlayer.position) / 3))));
                this.indiArmAction[2] = false;
                this.indiArmAction[0] = true;
            } else {
                this.indiArmAction[0] = false;
            }

            
            if (this.throwCharge >= 0.99) {
                console.log("throw");
                this.indiArmAction[0] = false;
                this.playerCloserTime = 0;
            }
        }
    }

    setTeam (team) {
        if (team == 0) {
            this.parts.forEach(part => {
                part.layer[4] = true;
                part.layer[5] = false;
            });
        } else if (team == 1) {
            this.parts.forEach(part => {
                part.layer[4] = false;
                part.layer[5] = true;
            });
        }
    }

    ragdoll (ragdoll) {
        if (this.ragdolled && !this.canGetUp || this.iframeTimer != -1) return;

        this.ragdolled = ragdoll;

        if (ragdoll) {
            if (this.heldBall != null) {
                this.heldBall.throw(this, new Vector2(0));
                this.heldBall = null;
                this.ballHoldLink.delete();
            }

            this.foregroundHand.layer[6] = this.foregroundHand.mask[6] = true;
            this.foregroundElbow.layer[6] = this.foregroundElbow.mask[6] = true;
            this.foregroundFoot.layer[7] = this.foregroundFoot.mask[7] = true;
            this.foregroundKnee.layer[7] = this.foregroundKnee.mask[7] = true;
            this.head.layer[7] = this.head.mask[7] = true;
            this.head.layer[8] = this.head.mask[8] = true;
            this.chest.layer[7] = this.chest.mask[7] = true;
            this.chest.layer[8] = this.chest.mask[8] = true;
            this.waist.layer[7] = this.waist.mask[7] = true;
            this.waist.layer[8] = this.waist.mask[8] = true;
            this.backgroundKnee.layer[8] = this.backgroundKnee.mask[8] = true;
            this.backgroundFoot.layer[8] = this.backgroundFoot.mask[8] = true;
            this.backgroundElbow.layer[9] = this.backgroundElbow.mask[9] = true;
            this.backgroundHand.layer[9] = this.backgroundHand.mask[9] = true;

            this.pins.forEach(pin => {
                pin.setEnabled(false);
            });

            this.bodyLinks.forEach(link => {
                link.minDistance = link.maxDistance;
            });

            this.focusPoint = this.chest;
        } else {
            this.gettingUp = 0;
            this.foregroundHand.layer[6] = this.foregroundHand.mask[6] = false;
            this.foregroundElbow.layer[6] = this.foregroundElbow.mask[6] = false;
            this.foregroundFoot.layer[7] = this.foregroundFoot.mask[7] = false;
            this.foregroundKnee.layer[7] = this.foregroundKnee.mask[7] = false;
            this.head.layer[7] = this.head.mask[7] = false;
            this.head.layer[8] = this.head.mask[8] = false;
            this.chest.layer[7] = this.chest.mask[7] = false;
            this.chest.layer[8] = this.chest.mask[8] = false;
            this.waist.layer[7] = this.waist.mask[7] = false;
            this.waist.layer[8] = this.waist.mask[8] = false;
            this.backgroundKnee.layer[8] = this.backgroundKnee.mask[8] = false;
            this.backgroundFoot.layer[8] = this.backgroundFoot.mask[8] = false;
            this.backgroundElbow.layer[9] = this.backgroundElbow.mask[9] = false;
            this.backgroundHand.layer[9] = this.backgroundHand.mask[9] = false;

            this.pins.forEach(pin => {
                pin.setEnabled(true);
            });

            this.bodyLinks.forEach(link => {
                link.minDistance = 0;
            });

            this.parts.forEach(part => {
                part.layer[4] = false;
                part.layer[5] = false;
            });
            this.iframeTimer = 0;

            this.getUpStart = [];
            this.getUpStart.push(this.head.position.copy);
            this.getUpStart.push(this.chest.position.copy);
            this.getUpStart.push(this.waist.position.copy);
            this.getUpStart.push(this.foregroundHand.position.copy);
            this.getUpStart.push(this.backgroundHand.position.copy);
            this.getUpStart.push(this.foregroundFoot.position.copy);
            this.getUpStart.push(this.backgroundFoot.position.copy);
        }
    }

    draw() {
        ctx0.lineCap = "round";

        if (this.iframeTimer != -1) {
            ctx0.globalAlpha = Math.cos((this.iframeTimer * this.iframeTimer * this.iframeTimer) / 1.5) / 3 + 2 / 3;
        }

        ctx0.strokeStyle = `hsl(${teamColors[this.team]}, 93%, 40%)`;
        ctx0.lineWidth = this.backgroundElbow.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.chest.position.x, this.chest.position.y);
        ctx0.lineTo(this.backgroundElbow.position.x, this.backgroundElbow.position.y);
        ctx0.stroke();

        ctx0.lineWidth = this.backgroundHand.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.backgroundElbow.position.x, this.backgroundElbow.position.y);
        ctx0.lineTo(this.backgroundHand.position.x, this.backgroundHand.position.y);
        ctx0.stroke();

        ctx0.strokeStyle = `hsl(${teamColors[this.team]}, 93%, 45%)`;
        ctx0.lineWidth = this.backgroundKnee.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.waist.position.x, this.waist.position.y);
        ctx0.lineTo(this.backgroundKnee.position.x, this.backgroundKnee.position.y);
        ctx0.stroke();

        ctx0.lineWidth = this.backgroundFoot.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.backgroundKnee.position.x, this.backgroundKnee.position.y);
        ctx0.lineTo(this.backgroundFoot.position.x, this.backgroundFoot.position.y);
        ctx0.stroke();

        ctx0.strokeStyle = `hsl(${teamColors[this.team]}, 93%, 50%)`;
        ctx0.lineWidth = this.waist.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.waist.position.x, this.waist.position.y);
        ctx0.lineTo(this.chest.position.x, this.chest.position.y);
        ctx0.stroke();

        ctx0.lineWidth = this.head.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.head.position.x, this.head.position.y);
        ctx0.lineTo(this.head.position.x, this.head.position.y);
        ctx0.stroke();

        ctx0.strokeStyle = `hsl(${teamColors[this.team]}, 93%, 55%)`;
        ctx0.lineWidth = this.foregroundKnee.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.waist.position.x, this.waist.position.y);
        ctx0.lineTo(this.foregroundKnee.position.x, this.foregroundKnee.position.y);
        ctx0.stroke();

        ctx0.lineWidth = this.foregroundFoot.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.foregroundKnee.position.x, this.foregroundKnee.position.y);
        ctx0.lineTo(this.foregroundFoot.position.x, this.foregroundFoot.position.y);
        ctx0.stroke();

        ctx0.strokeStyle = `hsl(${teamColors[this.team]}, 93%, 60%)`;
        ctx0.lineWidth = this.foregroundElbow.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.chest.position.x, this.chest.position.y);
        ctx0.lineTo(this.foregroundElbow.position.x, this.foregroundElbow.position.y);
        ctx0.stroke();

        ctx0.lineWidth = this.backgroundHand.radius * 2;
        ctx0.beginPath();
        ctx0.moveTo(this.foregroundElbow.position.x, this.foregroundElbow.position.y);
        ctx0.lineTo(this.foregroundHand.position.x, this.foregroundHand.position.y);
        ctx0.stroke();

        ctx0.globalAlpha = 1;

        this.pinCircles.forEach(pinCircle => {
            ctx0.fillStyle = "#ffffff";
            ctx0.beginPath();
            ctx0.arc(pinCircle.position.x, pinCircle.position.y, 10, 0, 2 * Math.PI);
            //ctx0.fill();
        });

        ctx0.fillStyle = "#ffffff";
        ctx0.beginPath();
        ctx0.arc(this.ballPickup.position.x, this.ballPickup.position.y, this.ballPickup.radius, 0, 2 * Math.PI);
        //ctx0.fill();

        if (this.armAction == 1 && this.heldBall != null) {
            let drawPos = worldToScreen(this.ballPickup.position);
            ctx1.lineWidth = 15;
            ctx1.strokeStyle = "#000000";
            ctx1.moveTo(drawPos.x + 20, drawPos.y);
            ctx1.arc(drawPos.x, drawPos.y, 15, 0, 2 * Math.PI * this.throwCharge);
            ctx1.stroke();
            
            ctx1.lineWidth = 10;
            ctx1.strokeStyle = "#ff00ff";
            ctx1.moveTo(drawPos.x + 20, drawPos.y);
            ctx1.arc(drawPos.x, drawPos.y, 15, 0, 2 * Math.PI * this.throwCharge);
            ctx1.stroke();
        }
    }
}

class Dodgeball {
    constructor (position, velocity = new Vector2(0), team = 0) {
        this.circle = new Circle(position, 30, ballWeight, 0.5, [], [true]);
        this.circle.velocity = velocity;
        this.circle.onCollide = this.hitSomething.bind(this);
        this.circle.type = 3;
        this.circle.owner = this;

        this.team = team;
        this.setTeam(this.team);
        this.thrower;

        this.colors = [];
        this.colors[-1] = ctx0.createLinearGradient(-this.circle.radius * 1.5, -this.circle.radius * 2, this.circle.radius / 2, this.circle.radius);
        this.colors[-1].addColorStop(0, "#ffffff");
        this.colors[-1].addColorStop(1, `#aaaaaa`);
        this.colors[0] = ctx0.createLinearGradient(-this.circle.radius * 1.5, -this.circle.radius * 2, this.circle.radius / 2, this.circle.radius);
        this.colors[0].addColorStop(0, "#ffffff");
        this.colors[0].addColorStop(1, `hsl(${teamColors[0]}, 93%, 60%)`);
        this.colors[1] = ctx0.createLinearGradient(-this.circle.radius * 1.5, -this.circle.radius * 2, this.circle.radius / 2, this.circle.radius);
        this.colors[1].addColorStop(0, "#ffffff");
        this.colors[1].addColorStop(1, `hsl(${teamColors[1]}, 93%, 60%)`);
    }

    throw(who, velocity) {
        this.thrower = who;
        this.circle.velocity = velocity;
        this.circle.mask[0] = true;
    }

    setTeam (team) {
        this.team = team;
        if (team == -1) {
            this.circle.mask[1] = true;
            this.circle.mask[2] = false;
            this.circle.mask[3] = false;
            this.circle.mask[4] = false;
            this.circle.mask[5] = false;

            this.circle.layer[1] = true;
            this.circle.layer[2] = false;
            this.circle.layer[3] = false;
        } else if (team == 0) {
            this.circle.mask[1] = false;
            this.circle.mask[2] = false;
            this.circle.mask[3] = true;
            this.circle.mask[4] = false;
            this.circle.mask[5] = true;

            this.circle.layer[1] = false;
            this.circle.layer[2] = true;
            this.circle.layer[3] = false;
        } else if (team == 1) {
            this.circle.mask[1] = false;
            this.circle.mask[2] = true;
            this.circle.mask[3] = false;
            this.circle.mask[4] = true;
            this.circle.mask[5] = false;

            this.circle.layer[1] = false;
            this.circle.layer[2] = false;
            this.circle.layer[3] = true;
        }
    }

    hitSomething (object, type) {
        if (type == 0 && this.team != -1) {
            this.setTeam(-1);
            this.thrower = null;
        }
        if (type == 4 || type == 5) {
            object.owner.ragdoll(true);
            //object.owner.canGetUp = false;

            if (type == 4) {
                game.teamBPoints++;
            }
            if (type == 5) {
                game.teamAPoints++;
            }

            this.setTeam(-1);
        }
    }

    draw () {
        ctx0.fillStyle = this.colors[this.team];
        ctx0.save();
        ctx0.translate(this.circle.position.x, this.circle.position.y);
        ctx0.beginPath();
        ctx0.arc(0, 0, this.circle.radius, 0, 2 * Math.PI);
        ctx0.fill();
        ctx0.restore();
    }
}

class Map {
    constructor (center, barriers, teamASpawns, teamBSpawns, ballSpawns, teamANav, teamBNav) {
        this.center = center;
        this.barriers = barriers;
        this.teamASpawns = teamASpawns;
        this.teamBSpawns = teamBSpawns;
        this.ballSpawns = ballSpawns;
        this.teamANav = teamANav;
        this.teamBNav = teamBNav;
    }
}

class Nav {
    constructor (position, length, barrier) {
        this.position = position;
        this.length = length;
        this.center = this.position.addR(new Vector2(length / 2, 0));
        this.position2 = this.position.addR(new Vector2(length, 0));
        this.barrier = barrier;
    }
}

class GameManager {
    constructor () {
        this.teamAJail = [];
        this.teamBJail = [];
        this.teamAPlayers = [];
        this.teamBPlayers = [];
        this.teamAPoints = [];
        this.teamBPoints = [];
        this.players = [];
        this.dodgeballs = [];
        this.teamAPoints = 0;
        this.teamBPoints = 0;
        this.teamAPlayerCount = 4;
        this.teamBPlayerCount = 4;
        this.humanTeam = 0;
        this.matchTimer = 300000;
        this.ballMultiplier = 1;
        this.pointsToWin = 25;
        this.map = basic1;
        this.gameOver = false;
        this.winner = -1;

        this.scoreMode = 0; // 0: time, 1: score, 2: score + time

        this.backup;
    }

    reset () {
        this.teamAJail = [];
        this.teamBJail = [];
        this.teamAPlayers = [];
        this.teamBPlayers = [];
        this.teamAPoints = [];
        this.teamBPoints = [];
        this.players = [];
        this.dodgeballs = [];
        this.teamAPoints = 0;
        this.teamBPoints = 0;
        this.teamAPlayerCount = this.backup.teamAPlayerCount;
        this.teamBPlayerCount = this.backup.teamBPlayerCount;
        this.humanTeam = this.humanTeam;
        this.matchTimer = this.backup.matchTimer;
        this.ballMultiplier = this.ballMultiplier;
        this.pointsToWin = this.backup.pointsToWin;
        this.gameOver = this.backup.gameOver;
        this.winner = this.backup.winner;

        this.scoreMode = this.backup.scoreMode; // 0: time, 1: score, 2: score + time

        this.backup = null;
    }

    startGame () {
        this.backup = {"teamAPlayerCount": this.teamAPlayerCount,
        "teamBPlayerCount": this.teamBPlayerCount, 
        "humanTeam": this.humanTeam,
        "matchTimer": this.matchTimer,
        "ballMultiplier": this.ballMultiplier,
        "pointsToWin": this.pointsToWin,
        "gameOver": this.gameOver,
        "winner": this.winner,
        "scoreMode": this.scoreMode,
        };

        barriers = this.map.barriers;

        this.map.ballSpawns.forEach(spawn => {
            for (let i = 0; i < this.ballMultiplier; i++) {
                this.dodgeballs.push(new Dodgeball(spawn.subR(new Vector2(0, i * 100)), new Vector2(0, 0), -1));
            }
        });

        for (let i = 0; i < this.teamAPlayerCount; i++) {
            let player = new Person(this.map.teamASpawns[i % this.map.teamASpawns.length].addR(new Vector2(getRandomInt(-100, 100), 0)), 0, false);
            this.teamAPlayers.push(player);
            this.players.push(player);
        }

        for (let i = 0; i < this.teamBPlayerCount; i++) {
            let player = new Person(this.map.teamBSpawns[i % this.map.teamBSpawns.length].addR(new Vector2(getRandomInt(-100, 100), 0)), 1, false);
            this.teamBPlayers.push(player);
            this.players.push(player);
        }

        if (this.humanTeam == 0 || this.humanTeam == 2) {
            this.teamAPlayers[getRandomInt(0, this.teamAPlayerCount - 1)].human = true;
        }

        if (this.humanTeam == 1 || this.humanTeam == 2) {
            this.teamBPlayers[getRandomInt(0, this.teamBPlayerCount - 1)].human = true;
        }
    }

    gameLogic (dT) {
        ctx1.translate(canvas0.width / 2, 0);
        ctx1.scale(screenScale.x, screenScale.y);

        if (this.scoreMode != 1) {
            this.matchTimer -= dT * 1000;

            ctx1.fillStyle = "#ff0000";
            ctx1.fillRect(-200, 0, 100, 50);
            ctx1.fillStyle = "#000000";
            ctx1.fillRect(-100, 0, 200, 50);
            ctx1.fillStyle = "#0000ff";
            ctx1.fillRect(100, 0, 100, 50);

            ctx1.fillStyle = "#ffffff";
            ctx1.font = "normal 48px sans-serif";

            ctx1.fillText(timeConversion(this.matchTimer), -50, 40);
            ctx1.fillText(this.teamAPoints, -175, 40);
            ctx1.fillText(this.teamBPoints, 125, 40);
        } else {
            ctx1.fillStyle = "#ff0000";
            ctx1.fillRect(-100, 0, 100, 50);
            ctx1.fillStyle = "#0000ff";
            ctx1.fillRect(0, 0, 100, 50);

            ctx1.fillStyle = "#ffffff";
            ctx1.font = "normal 48px sans-serif";

            ctx1.fillText(this.teamAPoints, -75, 40);
            ctx1.fillText(this.teamBPoints, 25, 40);
        }

        switch (this.scoreMode) {
            case 0:
                if (this.matchTimer <= 0) {
                    this.gameOver = true;

                    if (this.teamAPoints > this.teamBPoints) {
                        this.winner = 0;
                    } else if (this.teamAPoints < this.teamBPoints) {
                        this.winner = 1;
                    } else {
                        this.winner = 2;
                    }
                }
                break;

            case 1:
                if (this.teamAPoints >= this.pointsToWin) {
                    this.gameOver = true;
                    this.winner = 0;
                }
        
                if (this.teamBPoints >= this.pointsToWin) {
                    this.gameOver = true;
                    this.winner = 1;
                }
                break;

            case 2:
                if (this.teamAPoints >= this.pointsToWin) {
                    this.gameOver = true;
                    this.winner = 0;
                }
        
                if (this.teamBPoints >= this.pointsToWin) {
                    this.gameOver = true;
                    this.winner = 1;
                }

                if (this.matchTimer <= 0) {
                    this.gameOver = true;

                    if (this.teamAPoints > this.teamBPoints) {
                        this.winner = 0;
                    } else if (this.teamAPoints < this.teamBPoints) {
                        this.winner = 1;
                    } else {
                        this.winner = 2;
                    }
                }
                break;
        }
    }
}

// I shouldve made this earlier damn
class MenuButton {
    constructor (position, size, text, action) {
        this.position = position;
        this.size = size;
        this.text = text;
        this.action = action;
    }

    click () {
        this.action();
    }
}