let steps = 10;
let gravity = new Vector2(0, 980);

let barriers = [];
let circles = [];

function doPhysics(dT) {
    shuffle(circles);

    for (let i = 0; i < steps; i++) {
        let stepDT = dT / steps;

        for (let i = 0; i < circles.length; i++) {
            for (let j = i + 1; j < circles.length; j++) {
                circles[i].collideWithCircle(circles[j]);
            }
        }

        circles.forEach(circle => {
            circle.velocity.addS(gravity.mulR(new Vector2(stepDT * gravityMod)));
            circle.collideWithBarriers(barriers);
            circle.move(stepDT);
        });
    }
}

/* 
Collision layers
Objects will only collide if there is a layer in which one is on and the other has that mask
Same layer but not mask objects wont collide, same thing with same mask but not layer
As long as there is one shared layer+mask, they will collide

Layer list
I'm writing these down for my sanity

0: Barriers
1: Neutral/Dead balls
2: Team A balls
3: Team B balls
4: Team A
5: Team B
6: Ragdoll foreground arm
7: Ragdoll body, head and foreground leg
8: Ragdoll body, head and background leg
9: Ragdoll background arm
10: Team A barrier
11: Team B barrier
*/

/*
Link types

0: normal spring
1: force object to be at pinned object position
*/

/*
Collision object types

0: barrier
1: circle
2: ball pickup circle
3: ball
4: team A
5: team B
*/

/*
Game Modes
idk where to put these but theyll help later

-1: menu
0: controls
1: game setup
2: play
3: end screen
*/